CREATE DATABASE rise;
USE rise;

CREATE TABLE product (
model CHAR(4),
maker CHAR(1),
type DECIMAL(6, 2)
);

ALTER TABLE product
ALTER COLUMN type VARCHAR(7);

CREATE TABLE printer (
code INT,
model CHAR(4),
price DECIMAL(6, 2)
);

INSERT INTO product
VALUES ('abcd', 'A', 'chair'),
('abce', 'b', 'desk'),
('abdc', 'A', 'pen');

INSERT INTO printer
VALUES (11, 'ABCD', 200.30),
(220, 'DELL', 1200.30),
(50, 'HP', 500.50),
(225, 'DELL', 700.80);

ALTER TABLE printer
ADD type VARCHAR(6)
CHECK (type IN ('laser', 'matrix', 'jet'));

ALTER TABLE printer
ADD color CHAR(1) DEFAULT 'n'
CHECK (color IN ('y', 'n'));

INSERT INTO printer
VALUES (11, 'ABCD', 200.30, 'laser', 'y'),
(220, 'DELL', 1200.30, 'matrix', 'y'),
(50, 'HP', 500.50, 'jet', 'n'),
(225, 'DELL', 700.80, 'jet', 'y');


ALTER TABLE printer
DROP CONSTRAINT CK__printer__type__24927208;

ALTER TABLE printer
DROP COLUMN type;

DROP TABLE printer;
DROP TABLE product;