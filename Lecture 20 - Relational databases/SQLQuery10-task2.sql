CREATE DATABASE Flights;
USE Flights;

CREATE TABLE airline(
id INT PRIMARY KEY IDENTITY,
code VARCHAR(3) NOT NULL,
name VARCHAR(30) NOT NULL UNIQUE,
country VARCHAR(30) NOT NULL
);


CREATE TABLE airport(
id INT PRIMARY KEY IDENTITY,
code VARCHAR(5) NOT NULL,
name VARCHAR(50) NOT NULL UNIQUE,
country VARCHAR(30) NOT NULL,
city VARCHAR(30) NOT NULL
);

ALTER TABLE airport
ADD CONSTRAINT UQ_name UNIQUE (name,country);

CREATE TABLE airplane(
id INT PRIMARY KEY IDENTITY,
code VARCHAR(5) NOT NULL,
type VARCHAR(30) NOT NULL,
seats INT NOT NULL,
year INT NOT NULL,
CHECK (seats > 0),
CHECK (year >= 1900 AND year LIKE '____')
);


CREATE TABLE flight(
id INT PRIMARY KEY IDENTITY,
fnumber VARCHAR(10) NOT NULL,
airline_operator VARCHAR(5) NOT NULL,
dep_airport VARCHAR(5) NOT NULL,
arr_airport VARCHAR(5) NOT NULL,
flight_time TIME NOT NULL,
flight_duration INT,
airplane VARCHAR(5) NOT NULL
);


CREATE TABLE customer(
id INT PRIMARY KEY,
fname VARCHAR(50) NOT NULL ,
lname VARCHAR(50) NOT NULL,
email VARCHAR(50) NOT NULL UNIQUE,
CHECK (LEN(email)>=6 AND email LIKE '_%@_%._%')
);


CREATE TABLE agency(
id INT PRIMARY KEY IDENTITY,
name VARCHAR(30) NOT NULL,
country VARCHAR(30) NOT NULL,
city VARCHAR(30) NOT NULL,
phone VARCHAR(20),
CONSTRAINT UQ_agency_name UNIQUE (name, country)
);


CREATE TABLE booking(
id INT PRIMARY KEY IDENTITY,
code VARCHAR(10) NOT NULL,
agency VARCHAR(30) NOT NULL,
airline_code VARCHAR(5) NOT NULL,
flight_number VARCHAR(10) NOT NULL,
customer_id INT NOT NULL,
booking_date DATE NOT NULL,
flight_date DATE NOT NULL,
price DECIMAL(6,0) NOT NULL,
status BIT NOT NULL,
CHECK (booking_date <= flight_date),
CHECK (customer_id >= 1)
);


INSERT INTO airline
VALUES
  ('AZ', 'Alitalia', 'Italy'),
  ('BA', 'British Airways', 'United Kingdom'),
  ('LH', 'Lufthansa', 'Germany'),
  ('SR', 'Swissair', 'Switzerland'),
  ('FB', 'Bulgaria Air', 'Bulgaria'),
  ('AF', 'Air France', 'France'),
  ('TK', 'Turkish Airlines', 'Turkey'),
  ('AA', 'American Airlines', 'United States');
 
INSERT INTO airport
VALUES
  ('SOF', 'Sofia International', 'Bulgaria', 'Sofia'),
  ('CDG', 'Charles De Gaulle', 'France', 'Paris'),
  ('ORY', 'Orly', 'France', 'Paris'),
  ('LBG', 'Le Bourget', 'France', 'Paris'),
  ('JFK', 'John F Kennedy International', 'United States', 'New York'),
  ('ORD', 'Chicago O''Hare International','United States', 'Chicago'),
  ('FCO', 'Leonardo da Vinci International','Italy', 'Rome'),
  ('LHR', 'London Heathrow', 'United Kingdom', 'London');
 
INSERT INTO airplane
VALUES
  ('319', 'Airbus A319', 150, 1993),
  ('320', 'Airbus A320', 280, 1984),
  ('321', 'Airbus A321', 150, 1989),
  ('100', 'Fokker 100', 80, 1991),
  ('738', 'Boeing 737-800', 90, 1997),
  ('735', 'Boeing 737-800', 90, 1995);
 
INSERT INTO flight
VALUES
  ('FB1363', 'AZ', 'SOF', 'ORY', '12:45', 100, '738'),
  ('FB1364', 'AZ', 'CDG', 'SOF', '10:00', 120, '321'),
  ('SU2060', 'AZ', 'LBG', 'SOF', '11:10', 110, '738'),
  ('SU2061', 'TK', 'SOF', 'JFK', '13:00', 110, '320'),
  ('FB363', 'FB', 'SOF', 'ORD', '15:10', 110, '738'),
  ('FB364', 'FB', 'LHR', 'SOF', '21:05', 120, '738');
 
INSERT INTO customer
VALUES
  (1, 'Petar', 'Milenov', 'petter@mail.com'),
  (2, 'Dimitar', 'Petrov', 'petrov@mail.com'),
  (3, 'Ivan', 'Ivanov', 'ivanov@mail.com'),
  (4, 'Petar', 'Slavov', 'slavov@mail.com'),
  (5, 'Bogdan', 'Bobov', 'bobov@mail.com');
 
INSERT INTO agency
VALUES
  ('Travel One', 'Bulgaria', 'Sofia', '0783482233'),
  ('Travel Two', 'Bulgaria', 'Plovdiv', '02882234'),
  ('Travel Tour', 'Bulgaria', 'Sofia', NULL),
  ('Aerotravel', 'Bulgaria', 'Varna', '02884233');
 
INSERT INTO booking
VALUES
  ('YN298P', 'Travel One', 'FB', 'FB1363', 1, '2013-11-18', '2013-12-25', 300, 0),
  ('YA298P', 'Travel Two', 'FB', 'FB1364', 2, '2013-12-18', '2013-12-25', 300, 1),
  ('YB298P', 'Travel Tour', 'FB', 'SU2060', 3, '2014-01-18', '2014-02-25', 400, 0),
  ('YC298P', 'Travel One', 'FB', 'SU2061', 4, '2014-11-11', '2014-11-25', 350, 0),
  ('YD298P', 'Travel Tour', 'FB', 'FB363', 1, '2013-11-03', '2013-12-20', 250, 1),
  ('YE298P', 'Aerotravel', 'FB', 'FB364', 2, '2013-11-07', '2013-12-21', 150, 0);
