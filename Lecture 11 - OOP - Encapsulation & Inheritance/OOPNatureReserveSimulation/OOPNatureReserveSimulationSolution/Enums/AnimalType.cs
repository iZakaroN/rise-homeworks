﻿namespace OOPNatureReserveSimulationSolution.Enums
{
    public enum AnimalType
    {
        None,
        Herbivorous,
        Carnivorous,
        Omnivorous

    }
}
