﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Contracts;
using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution.Bioms
{
    public abstract class Biome
    {
        protected Biome(int capacity, IMap map)
        {
            Capacity = capacity;
            Map = map;
        }

        public virtual BiomType Type { get; init; } = BiomType.None;
        public int Capacity { get; init; }
        public IMap Map { get; set; }
        public virtual List<Animal> Animals { get; set; }
        public virtual List<IEatable> Foods { get; set; }
    }
}
