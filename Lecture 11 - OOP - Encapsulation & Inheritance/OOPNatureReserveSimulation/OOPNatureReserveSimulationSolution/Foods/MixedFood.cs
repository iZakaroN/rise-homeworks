﻿namespace OOPNatureReserveSimulationSolution.Foods
{
    public class MixedFood
    {
        public static readonly HashSet<Food> MixedFoodYoung = new HashSet<Food>() {
            new Food("leaves",3),
            new Food("seeds",4),
            new Food("fruits",5),
            new Food("milk",4) };

        public static readonly HashSet<Food> MixedFoodMiddle = new HashSet<Food>() {
            new Food("leaves",3),
            new Food("plant",7),
            new Food("nuts",6),
            new Food("fruits",5),
            new Food("bark",3),
            new Food("honey",9),
            new Food("eggs",10),
            new Food("fresh meat",11),
            new Food("small animals",15),
            new Food("human food",8),
            new Food("mouse", 15),
            new Food("eagle", 20)};

        public static readonly HashSet<Food> MixedFoodAdult = new HashSet<Food>() {
            new Food("leaves",3),
            new Food("fruits",5),
            new Food("big plants",8),
            new Food("bark",3),
            new Food("honey",9),
            new Food("eggs",10),
            new Food("fresh meat",11),
            new Food("meat",12),
            new Food("small animals",15),
            new Food("big animals",15),
            new Food("human food",8),
            new Food("mouse", 15),
            new Food("wolf", 20),
            new Food("eagle", 20),
            new Food("deer", 15),
            new Food("pig", 25),};
    }
}