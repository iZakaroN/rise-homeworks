﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Bioms;

namespace OOPNatureReserveSimulationSolution.Map
{
    public class Tile
    {
        public Tile(Biome biome, (int x, int y) coordinates)
        {
            this.Biome = biome;
            this.Coordinates = coordinates;
        }

        public (int x, int y) Coordinates { get; set; }
        public Biome Biome { get; set; }

        public void AddAnimal(Animal animal)
        {
            if (Biome.Animals.Count < Biome.Capacity)
            {
                Biome.Animals.Add(animal);
                Biome.Foods.Add(animal);
            }
        }
        public void RemoveAnimal(Animal animal)
        {
            Biome.Animals.Remove(animal);
            Biome.Foods.Remove(animal);
        }
    }
}
