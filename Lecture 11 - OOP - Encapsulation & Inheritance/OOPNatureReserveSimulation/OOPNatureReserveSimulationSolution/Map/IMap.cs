﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Contracts;

namespace OOPNatureReserveSimulationSolution.Map
{
    public interface IMap
    {
        public int Rows { get; }
        public int Cols { get; }
        public Tile GetTile(int x, int y);
        public Tile[,] GetTiles();
        public List<Animal> GetAnimals();
        public List<IEatable> GetFoods();
        public Tile GetTile(Animal animal);
    }
}
