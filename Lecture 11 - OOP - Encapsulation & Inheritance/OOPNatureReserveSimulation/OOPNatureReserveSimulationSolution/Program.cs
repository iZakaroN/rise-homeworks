﻿using OOPNatureReserveSimulationSolution.IO;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution
{
    public class Program
    {
        static void Main(string[] args)
        {
            IWriter fileWriter = new FileWriter();
            IWriter consoleWriter = new ConsoleWriter();
            IMap map = new Map.Map(3, 3);
            NatureEcosystem natureEcosystem = new NatureEcosystem(consoleWriter, map);
            //natureEcosystem.RunSimulation(true);
            natureEcosystem.LiveSimulation(false, 10);
        }
    }
}