﻿using OOPNatureReserveSimulationSolution.Animals;

namespace OOPNatureReserveSimulationSolution.Contracts
{
    public interface IBehaviour
    {
        string PartiallyEat(Animal animal, IEatable food);

        string CompletelyEat(Animal animal, IEatable food);

        string NotHungry(Animal animal);

        string NotEat(Animal animal, IEatable food);

        string Starve(Animal animal);

        string Hungry(Animal animal);

        string Die(Animal animal);
    }
}
