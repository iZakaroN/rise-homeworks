﻿namespace OOPNatureReserveSimulationSolution.IO
{
    public interface IWriter
    {
        void WriteLine(object message);
    }
}
