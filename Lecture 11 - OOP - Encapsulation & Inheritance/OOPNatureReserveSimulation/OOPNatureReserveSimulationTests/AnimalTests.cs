﻿using Moq;
using OOPNatureReserveSimulationSolution.Contracts;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationTests
{
    [TestFixture]
    public class AnimalTests
    {
        private const string ExpectedNotHungryMessage = "Animal None with name wolf: I'm not hungry anymore. I'm so sleepy...";

        [Test]
        public void Feed_PassEatableFood_MustNotBeHungry()
        {
            var mapMock = new Mock<IMap>();
            var animalBehaviour = new Mock<IBehaviour>();
            
            var animal = new TestAnimal(
                "wolf",
                6,
                new HashSet<Food> { new Food("fresh meat", 11) },
                20,
                mapMock.Object);
            animalBehaviour.Setup(animalBehaviour => animalBehaviour.NotHungry(animal)).Returns(ExpectedNotHungryMessage);

            animal.Feed(new Food("fresh meat", 11));

            Assert.That(animalBehaviour.Object, Is.Not.Null);
            Assert.That(animalBehaviour.Object.NotHungry(animal), Is.EqualTo(ExpectedNotHungryMessage));
            //animalBehaviour.Verify<string>((animalBehaviour) => animalBehaviour.NotHungry(animal), Expected);
        }
    }
}
