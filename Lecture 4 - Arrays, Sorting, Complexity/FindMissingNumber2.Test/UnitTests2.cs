using NUnit.Framework;
using static FindMissingNumber2.MissingNumber2;

namespace FindMissingNumber2.Test
{
    [TestFixture]
    public class Tests
    {
        [TestCase(new int[] { 1 })]
        [TestCase(new int[] { })]
        public void FindMissingThrowsExceptionWhenArrayLengthIsLessOrEqualTo1(int[] arr)
        {
            Assert.Throws<ArgumentException>(() => FindMissing(arr));
        }

        [Test]
        public void FindMissingWorksCorrectWith2Numbers()
        {
            int[] arr = { 1, 3 };

            Assert.That(FindMissing(arr), Is.EqualTo(2));
        }

        [Test]
        public void FindMissingWorksCorrectWithMoreNumbers()
        {
            int[] arr = { 4, 2, 5, 7, 3 };

            Assert.That(FindMissing(arr), Is.EqualTo(6));
        }
    }
}