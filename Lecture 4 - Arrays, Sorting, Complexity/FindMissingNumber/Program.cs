﻿namespace FindMissingNumber
{
    public class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class MissingNumber
    {
        public static int FindMissing(int[] arr)
        {
            int n = arr.Length;

            if (n <= 1)
            {
                throw new ArgumentException("Input is not valid.");
            }

            BubbleSort(arr);

            int missingNumber = arr[0];
            for (int i = 1; i < n; i++)
            {
                if (arr[i] - arr[i - 1] > 1)
                {
                    return arr[i] - 1;
                }
            }

            return missingNumber;
        }

        private static void BubbleSort(int[] arr)
        {
            int n = arr.Length;
            bool isSwapped;

            for (int i = 0; i < n - 1; i++)
            {
                isSwapped = false;

                for (int j = 0; j < n - i - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                        isSwapped = true;
                    }
                }

                if (isSwapped == false)
                {
                    return;
                }
            }
        }
    }
}