﻿namespace FindCubicRoot
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(int.MaxValue);
            Console.WriteLine(Math.Pow(1290, 3));
        }
    }

    public class Root
    {
        public static int FindRoot3(int x)
        {
            if (x < 0)
            {
                throw new ArgumentException("Number must be positive");
            }

            if (x == 0 || x == 1)
            {
                return x;
            }

            int start = 0;
            int end = x;

            while (true)
            {
                int mid = (end + start) / 2;
                int diff = mid * mid * mid - x;

                if (diff == 0)
                {
                    return mid;
                }

                if (diff > 0)
                {
                    end = mid;
                }
                else
                {
                    start = mid;
                }
            }
        }

        // Another decision

        private static Dictionary<int, int> numbers;

        public Root()
        {
            numbers = new Dictionary<int, int>();
        }

        public static int Root3(int x)
        {
            const int MaxNumber = 1290;

            if (x < 0)
            {
                throw new ArgumentException("Number must be positive");
            }

            //Dictionary<int, int> numbers = new Dictionary<int, int>();

            for (int i = 0; i <= MaxNumber; i++)
            {
                numbers[i] = (int)Math.Pow(i, 3);
            }

            return numbers.FirstOrDefault(n => n.Value == x).Key;
        }
    }
}