﻿namespace P04.Expression
{
    public class Program
    {
        static void Main(string[] args)
        {
            string input = "AB3(DC)2(F)2(E3(G))";
            string output = Expression.ExpressionExpansion(input);
            Console.WriteLine(output);
        }
    }

    public class Expression
    {
        public static string ExpressionExpansion(string expression)
        {
            string result = "";
            int n = expression.Length;

            for (int i = 0; i < n; i++)
            {
                char c = expression[i];

                if (char.IsDigit(c))
                {
                    int count = c - '0';
                    int j = i + 1;

                    while (j < n && expression[j] != ')')
                    {
                        j++;
                    }

                    string subExpression = expression.Substring(i + 2, j - i - 2);
                    string expandedString = ExpressionExpansion(subExpression);

                    for (int k = 0; k < count; k++)
                    {
                        result += expandedString;
                    }
                    i = j;
                }
                else
                {
                    if (c != ')')
                    {
                        result += c;
                    }
                }
            }

            return result;
        }
    }
}