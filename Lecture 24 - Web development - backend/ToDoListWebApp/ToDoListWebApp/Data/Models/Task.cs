﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoListWebApp.Data.Models
{
    [Table("Task")]
    public partial class Task
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; }

        [Required]
        [StringLength(1000)]
        [Unicode(false)]
        public string Description { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool Done { get; set; }

        public int AssignmentId { get; set; }

        [ForeignKey("AssignmentId")]
        [InverseProperty("Tasks")]
        public virtual Assignment Assignment { get; set; }
    }
}