using L03_Team;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestFibonacciTask
{
    [TestClass]
    public class TestFibonacciMatrix
    {
        [TestMethod]
        public void TestOutputGivenFibonacciMatrix3x3()
        {
            // Arrange
            int[,] input = { { 0, 1, 1 }, { 13, 100, 2 }, { 8, 5, 3 } };

            //Act 
            bool actual = L03_Team.MatrixClass.FibonacciMatrix(input);

            // Assert
            Assert.AreEqual(true, actual);
        }


        [TestMethod]
        public void TestOutputGivenNotFibonacciMatrix3x3()
        {
            // Arrange
            int[,] input = { { 1, 100, 2 }, { 21, 100, 3 }, { 13, 8, 5 } };

            //Act 
            bool actual = MatrixClass.FibonacciMatrix(input);

            // Assert
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void TestOutputGivenFibonacciSquare2x2In3x3Matrix()
        {
            // Arrange
            int[,] input = { { 1, 1, 100 }, { 3, 2, 100 }, { 100, 100, 100 } };

            //Act 
            bool actual = MatrixClass.FibonacciMatrix(input);

            // Assert
            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void TestSortGivenNormalInput()
        {
            // Arrange
            int[] input = { 1, 13, 8 };
            string expected = "1 8 13";
            string actual = MatrixClass.SortDictionaryByOrder(input);        

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSortGiven0()
        {
            // Arrange
            int[] input = { };

            // Assert
            Assert.ThrowsException<Exception>(() => MatrixClass.SortDictionaryByOrder(input), "Expected exception was not thrown.");
        }

        [TestMethod]
        public void TestSortGiven1()
        {
            // Arrange
            int[] input = { 8 };

            // Assert
            Assert.ThrowsException<Exception>(() => MatrixClass.SortDictionaryByOrder(input), "Expected exception was not thrown.");
        }


        [TestMethod]
        public void TestFibonacciNeighboursGivenFibonacciMatrix3x3()
        {
            // Arrange
            int[,] input = { { 0, 1, 1 }, { 13, 100, 2 }, { 8, 5, 3 } };

            //Act 
            bool actual = L03_Team.MatrixClass.FibonacciNeighbours(input);

            // Assert
            Assert.AreEqual(true, actual);
        }


        [TestMethod]
        public void TestFibonacciNeighboursGivenNotFibonacciMatrix3x3()
        {
            // Arrange
            int[,] input = { { 1, 100, 2 }, { 21, 100, 3 }, { 13, 8, 5 } };

            //Act 
            bool actual = MatrixClass.FibonacciNeighbours(input);

            // Assert
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void TestFibonacciNeighboursGivenInnerFibonacciSquare3x3In5x5Matrix()
        {
            // Arrange
            int[,] input = { { 1, 1, 1, 1, 1 }, { 2, 3, 5, 1, 1 }, { 55, 100, 8, 1, 1 }, { 34, 21, 13, 1, 1 }, { 1, 1, 1, 1, 1 } };

            //Act 
            bool actual = MatrixClass.FibonacciNeighbours(input);

            // Assert
            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void TestFibonacciNeighboursGivenInnerNotFibonacciSquareIn5x5Matrix()
        {
            // Arrange
            int[,] input = { { 1, 1, 1, 1, 1 }, { 2, 3, 6, 1, 1 }, { 55, 100, 8, 1, 1 }, { 34, 21, 13, 1, 1 }, { 1, 1, 1, 1, 1 } };

            //Act 
            bool actual = MatrixClass.FibonacciNeighbours(input);

            // Assert
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void TestFibonacciNeighboursGivenTopLeftCornerFibonacciSequence2x2In3x3Matrix()
        {
            // Arrange
            int[,] input = { { 100, 2, 1 }, { 5, 3, 1 }, { 1, 1, 1 } };

            //Act 
            bool actual = MatrixClass.FibonacciNeighbours(input);

            // Assert
            Assert.AreEqual(true, actual);
        }

        //[TestMethod]
        //public void TestFibonacciNeighboursGivenTopRightCornerFibonacciSequence2x2In3x3Matrix()
        //{
        //    // Arrange
        //    int[,] input = { { 1, 3, 100 }, { 1, 2, 1 }, { 1, 1, 1 } };

        //    //Act 
        //    bool actual = MatrixClass.FibonacciNeighbours(input);

        //    // Assert
        //    Assert.AreEqual(true, actual);
        //}
    }
}