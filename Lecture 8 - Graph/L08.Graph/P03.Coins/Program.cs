﻿namespace P03.Coins
{
    public class Program
    {
        static void Main(string[] args)
        {
            Coin coin = new Coin(3);
            Console.WriteLine(coin.CollectCoins(0, new int[,] { { 0, 1 }, { 1, 2 }, { 0, 2 } }));

            //Coin coin = new Coin(3);
            //Console.WriteLine(coin.CollectCoins(2, new int[,] { { 0, 1 }, { 1, 0 } }));
        }
    }
}