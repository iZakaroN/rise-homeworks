using NUnit.Framework;

namespace _02.FormFields.Test
{
    public class Tests
    {
        [Test]
        public void CalculateFieldReturnsTrueWhenGrapfIsAcyclicAndRightNodesAreGiven()
        {
            int[] allFields = new int[] { 6, 1, 2, 7, 4, 5 };
            int[] givenFields = new int[] { 1, 2, 4, 5 };
            int[,] nodeEdges = new int[,] { { 6, 1, 2 }, { 7, 4, 5 } };

            Assert.That(FormField.CalculateField(allFields, givenFields, nodeEdges), Is.True);
        }

        [Test]
        public void CalculateFieldReturnsTrueWhenGrapfIsAcyclicAndSectionRightAreGivenAndSectionLeftAny()
        {
            int[] allFields = new int[] { 5, 1, 6, 3, 4 };
            int[] givenFields = new int[] { 1, 3, 4 };
            int[,] nodeEdges = new int[,] { { 5, 1, 6 }, { 6, 3, 4 } };

            Assert.That(FormField.CalculateField(allFields, givenFields, nodeEdges), Is.True);
        }

        [Test]
        public void CalculateFieldReturnsFalseWhenGrapfIsAcyclicAndSectionRightAreGivenAndSectionLeftNothing()
        {
            int[] allFields = new int[] { 4, 1, 5, 3 };
            int[] givenFields = new int[] { 1, 3 };
            int[,] nodeEdges = new int[,] { { 4, 1, 5 }, { 5, 4, 3 } };

            Assert.That(FormField.CalculateField(allFields, givenFields, nodeEdges), Is.False);
        }

        [Test]
        public void CalculateFieldReturnsFalseWhenGrapfIsCyclic()
        {
            int[] allFields = new int[] { 3,1,4 };
            int[] givenFields = new int[] { 1 };
            int[,] nodeEdges = new int[,] { { 3, 1, 4 }, { 4, 3, 1 } };

            Assert.That(FormField.CalculateField(allFields, givenFields, nodeEdges), Is.False);
        }
    }
}