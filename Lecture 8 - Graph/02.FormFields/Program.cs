﻿namespace _02.FormFields
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(FormField.CalculateField(
                new int[] { 6, 1, 2, 7, 4, 5 },
                new int[] { 1, 2, 4, 5 },
                new int[,] { { 6, 1, 2 }, { 7, 4, 5 } }));

            Console.WriteLine(FormField.CalculateField(
                new int[] { 5, 1, 6, 3, 4 },
                new int[] { 1, 3, 4 },
                new int[,] { { 5, 1, 6 }, { 6, 3, 4 } }));

            Console.WriteLine(FormField.CalculateField(
                new int[] { 4, 1, 5, 3 },
                new int[] { 1, 3 },
                new int[,] { { 4, 1, 5 }, { 5, 4, 3 } }));
        }
    }
}