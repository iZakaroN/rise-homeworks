using NUnit.Framework;

namespace P01.CarNumbers.Test
{
    public class Tests
    {
        private Dictionary<string, string> carNumberOwner;

        [SetUp]
        public void Setup()
        {
            carNumberOwner = new Dictionary<string, string>();

            carNumberOwner.Add("CA1234", "Petyr Petrov");
            carNumberOwner.Add("CA1235", "Ivan Petrov");
            carNumberOwner.Add("CA1236", "Petyr Petrov");
            carNumberOwner.Add("CA1237", "Anna Popova");
            carNumberOwner.Add("CA1238", "Petyr Petrov");
            carNumberOwner.Add("CA1239", "Ivan Petrov");
        }

        [Test]
        public void GetCarOwnerThrowsExceptionWhenCarNumberDoesNotExist()
        {
            Assert.Throws<KeyNotFoundException>(() => CarNumbers.GetCarOwner(carNumberOwner, "123456"));
        }

        [Test]
        public void GetCarOwnerReturnsOwnerWhenCarNumberExists()
        {
            Assert.That(CarNumbers.GetCarOwner(carNumberOwner, "CA1234"), Is.EqualTo("Petyr Petrov"));
        }

        [Test]
        public void GetOwnersWithMoreCarsReturnsOwners()
        {
            Assert.That(CarNumbers.GetOwnersWithMoreCars(carNumberOwner), Is.EqualTo("Owners with more than 1 car: Petyr Petrov, Ivan Petrov"));
        }

        [Test]
        public void GetOwnersWithMoreCarsReturnsCorrectMessageWhenNoOwnerHasMoreThan1Car()
        {
            carNumberOwner.Remove("CA1236");
            carNumberOwner.Remove("CA1238");
            carNumberOwner.Remove("CA1239");

            Assert.That(CarNumbers.GetOwnersWithMoreCars(carNumberOwner), Is.EqualTo("No owners with more than 1 car."));
        }
    }
}