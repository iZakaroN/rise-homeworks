using NUnit.Framework;

namespace P02.ArrayIntersection.Test
{
    public class Tests
    {
        [Test]
        public void GetIntersectionWorksCorrectWithNotEmptyArrays()
        {
            int[] first = { 1, 2, 3, 4 };
            int[] second = { 1, 2 };

            Assert.That(Intersection.GetIntersection(first, second), Is.EqualTo(new int[] { 1, 2 }));
        }

        [Test]
        public void GetIntersectionWorksCorrectWithNotEmptyArraysThatHaveSameElements()
        {
            int[] first = { 1, 2, 3, 4 };
            int[] second = { 1, 2, 3, 4 };

            Assert.That(Intersection.GetIntersection(first, second), Is.EqualTo(new int[] { 1, 2, 3, 4 }));
        }

        [Test]
        public void GetIntersectionReturnsEmptyArrayWhenOneofTheInputArraysIsEmpty()
        {
            int[] first = { 1, 2, 3, 4 };
            int[] second = { };

            Assert.That(Intersection.GetIntersection(first, second), Is.EqualTo(new int[] { }));
        }
    }
}