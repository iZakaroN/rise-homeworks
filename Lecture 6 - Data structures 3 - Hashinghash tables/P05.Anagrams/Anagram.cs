﻿namespace P05.Anagrams
{
    public class Anagram
    {
        public static bool AreAnagram(string first, string second)
        {
            bool result = false;

            if (first.Length == second.Length)
            {
                SortedDictionary<char, int> charsCountFirst = new SortedDictionary<char, int>();
                SortedDictionary<char, int> charsCountSecond = new SortedDictionary<char, int>();

                CalculateCharsCount(first, charsCountFirst);
                CalculateCharsCount(second, charsCountSecond);

                if (charsCountFirst.SequenceEqual(charsCountSecond))
                {
                    result = true;
                }
            }

            return result;
        }

        private static void CalculateCharsCount(string str, SortedDictionary<char, int> charsCount)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (!charsCount.ContainsKey(str[i]))
                {
                    charsCount[str[i]] = 1;
                }
                else
                {
                    charsCount[str[i]]++;
                }
            }
        }
    }
}
