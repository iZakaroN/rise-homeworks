﻿namespace P03.NonRepeatingCharacters
{
    public class Character
    {
        public static char[] GetNonRepeatingChars(string input)
        {
            Dictionary<char, int> characterCount = new Dictionary<char, int>();
            int n = input.Length;

            for (int i = 0; i < n; i++)
            {
                if (!characterCount.ContainsKey(input[i]))
                {
                    characterCount[input[i]] = 1;
                }
                else
                {
                    characterCount[input[i]]++;
                }
            }

            char[] nonRepeating = characterCount.Where(x => x.Value == 1).Select(x => x.Key).ToArray();

            return nonRepeating;
        }

        public static int GetFirstUniqueCharacter(string input)
        {
            char[] uniqueChars = GetNonRepeatingChars(input);

            int result = -1;

            if (uniqueChars.Length != 0)
            {
                char firstUnique = uniqueChars[0];
                result = input.IndexOf(firstUnique);
            }

            return result;
        }
    }
}
