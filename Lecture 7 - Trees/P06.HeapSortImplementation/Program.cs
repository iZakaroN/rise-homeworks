﻿namespace P06.HeapSortImplementation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 17, 1, 2, 18, 10, 15, 13 };

            HeapSort<int> heap = new HeapSort<int>();
            heap.Sort(arr);

            Console.WriteLine(string.Join(", ",arr)); //1, 2, 10, 13, 15, 17, 18
        }
    }
}