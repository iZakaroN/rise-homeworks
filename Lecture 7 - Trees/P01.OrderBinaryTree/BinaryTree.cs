﻿namespace P01.OrderBinaryTree
{
    public class BinaryTree
    {
        List<BinaryTree> result;

        public BinaryTree(int value, BinaryTree left, BinaryTree right)
        {
            Value = value;
            Left = left;
            Right = right;
            result = new List<BinaryTree>();
        }

        public int Value { get; set; }

        public BinaryTree Left { get; set; }

        public BinaryTree Right { get; set; }

        public List<BinaryTree> DfsPreOrder(BinaryTree node)
        {
            if (node == null)
            {
                return result;
            }

            result.Add(node);

            DfsPreOrder(node.Left);
            DfsPreOrder(node.Right);

            return result;
        }

        public List<BinaryTree> DfsPostOrder(BinaryTree node)
        {
            if (node == null)
            {
                return result;
            }

            DfsPostOrder(node.Left);
            DfsPostOrder(node.Right);

            result.Add(node);

            return result;
        }

        public List<BinaryTree> DfsInOrder(BinaryTree node)
        {
            if (node == null)
            {
                return result;
            }

            DfsInOrder(node.Left);

            result.Add(node);

            DfsInOrder(node.Right);

            return result;
        }
    }
}
