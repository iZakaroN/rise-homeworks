using NUnit.Framework;

namespace P05.MergeSortedLists.Test
{
    public class Tests
    {
        int[,] lists;

        [SetUp]
        public void Setup()
        {
            lists = new int[,]
                {
                    { 1, 2, 3, 4 },
                    { 27, 37, 47, 57 },
                    { 15, 20, 25, 30 }
                };
        }

        [Test]
        public void MergeListsReturnsOrderedNumbers()
        {
            int[] expected = { 1, 2, 3, 4, 15, 20, 25, 27, 30, 37, 47, 57 };

            Assert.That(Merge.MergeLists(lists),Is.EqualTo(expected));
        }
    }
}